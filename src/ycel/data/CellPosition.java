package ycel.data;

import java.util.Comparator;


/**
 * Posição de uma célula.
 * 
 * <p>
 * Objectos desta classe devem ser imutáveis.
 * </p>
 */
public final class CellPosition implements Comparable<CellPosition> {

	/**
	 * string representante da coluna
	 */
	private final String coluna;
	
	/**
	 * número da linha
	 */
	private final int linha;

	/**
	 * Construtor.
	 * @param col Coluna.
	 * @param row Linha.
	 */
	public CellPosition(String col, int row) {
		this.coluna = col;
		this.linha = row;
	}

	/**
	 * Obtém coluna.
	 * @return Valor da coluna.
	 */
	public String getCol() {
		return coluna;
	}
	/**
	 * Obtém linha.
	 * @return Valor da linha.
	 */
	public int getRow() {
		return linha;
	}

	/**
	 * Compara com outra posição.
	 * 
	 * <p>
	 * A comparação entre posições deve
	 * ter em conta primeiro a ordem entre colunas
	 * definida por {@link #COLUMN_COMPARATOR}
	 * e depois a ordem entre (o valor inteiro das) linhas. 
	 * 
	 * </p>
	 * @param other A outra posição.
	 * @return Valor conforme descrito em {@link Comparable#compareTo(Object)}.
	 */
	@Override
	public int compareTo(CellPosition other) {
		
		if(COLUMN_COMPARATOR.compare(getCol(), other.getCol()) == 0){
			
			if(getRow() < other.getRow()){
				return -1;
				
			} else  if (getRow() > other.getRow()){
				return 1;
				
			} else {
				return 0;
			}
			
		} else {
			return COLUMN_COMPARATOR.compare(getCol(), other.getCol());
		}
		
	}

	/**
	 * Comparador de colunas.
	 * <p>
	 * O comparador deve permitir ordenar as colunas
	 * tendo em conta primeiro o tamanho das strings
	 * respectivas e só depois a  "ordem natural" 
	 * definida por {@link String#compareTo(String)}.
	 * (ex. para termos <code>A, B, ..., AB</code>
	 * e não <code>A, ..., AB, ..., B</code>).
	 * </p>
	 */
	public static final Comparator<String> COLUMN_COMPARATOR = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			
			if(o1.length() > o2.length()) {
				return 1;
				
			} else if(o1.length() < o2.length()) {
				return -1;
				
			} else {
				for (int i = 0; i < o1.length(); i++) {
					if(o1.charAt(i) < o2.charAt(i)){
						return -1;
						
					} else if(o1.charAt(i) > o2.charAt(i)){
						return 1;
					}
				}
				return 0;
			}

		}
	};


	/**
	 * Obtém representação textual.
	 * @return String que codifica a coluna seguida da linha.
	 */
	@Override
	public String toString() {
		return getCol() + getRow();
	}

	/** 
	 * Testa equivalência de conteúdo.
	 * @param o Outra referência. 
	 * @return <code>true</code> se e só se 
	 *   <code>o</code> se refere a um objecto
	 *   {@link CellPosition} com os mesmos
	 *   valores para linha e para a coluna.
	 */
	@Override
	public boolean equals(Object o) {
		
		if (o instanceof CellPosition){
		
			if(getCol() == ((CellPosition)o).getCol() && getRow() == ((CellPosition)o).getRow()){
				return true;
			}
		}
		return false;
	}
}
