package ycel.data;

/**
 * Referência a outra célula.
 * 
 * <p>
 * Este tipo de conteúdo é codificado
 * com uma fórmula <code>#p</code> 
 * onde <code>p</code>
 * é a posição
 * da célula referida, ex. <code>#A12</code>
 * para a célula na coluna <code>A</code>
 * e linha <code>12</code>.
 * </p>
 */
public final class CReference implements NumberContent {

	/**
	 * posição CellPosition à qual é feita a referência
	 */
	private CellPosition position;

	/**
	 * Construtor.
	 * @param pos Posição da outra célula.
	 */
	public CReference(CellPosition pos) {
		position = pos;
	}
	/**
	 * Obtém formula. 
	 * @return String iniciada por <code>#</code>
	 *   seguida da string a descrever a posição.
	 */
	@Override
	public String formula() {
		return "#" + position.toString();
	}

	/**
	 * Avalia referência.
	 * @return Valor de avaliar a referência
	 *    caso o seu conteúdo seja um objecto
	 *    {@link NumberContent}, ou {@link Double#NaN}
	 *    caso contrário.
	 */
	@Override
	public Double evaluate(CellValues cells) {
		
		if(cells.getContent(position) instanceof NumberContent){
			return ((NumberContent) cells.getContent(position)).evaluate(cells);
		}
		return Double.NaN;
	}
}
