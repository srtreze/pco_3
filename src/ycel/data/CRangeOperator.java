package ycel.data;

import java.util.List;

/**
 * Enumeração de operadores de avaliação para 
 * {@link CRangeOperation}.
 *  
 * <p>
 * Cada elemento na enumeração define 
 * uma implementação do método {@link #evaluate(List) double evaluate(List&lt;Double&gt;)}
 * que toma como argumento uma lista de valores e devolve
 * um resultado.
 * </p>
 *
 */
public enum CRangeOperator {
	/** Média */
	AVG {
		@Override
		double evaluate(List<Double> list) {
			Double soma = 0.0;
			for (Double d : list) {
				soma += d;
			}
			return soma/list.size();
		} 
	},
	/** Mínimo. */
	MIN {
		@Override
		double evaluate(List<Double> list) {

			Double minimo = Double.MAX_VALUE;
			
			for (Double d : list) {
				minimo = Math.min(minimo, d);
			}
			return minimo;
		} 
	}, 
	/** Soma. */
	SUM {
		@Override
		double evaluate(List<Double> list) {
			Double soma = 0.0;
			for (Double d : list) {
				soma += d;
			}
			return soma;
		} 
	}, 
	/** Máximo */
	MAX {
		@Override
		double evaluate(List<Double> list) {
			Double maximus = Double.MIN_VALUE;
			
			for (Double d : list) {
				maximus = Math.max(maximus, d);
			}
			return maximus;
		} 
	};

	/**
	 * Avalia lista de valores.
	 * @param list Lista de valores. 
	 * @return Resultado da avaliar a lista.
	 */
	abstract double evaluate(List<Double> list);
}
