package ycel.data;


import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;


/**
 * Operação sobre gama de células.
 * 
 * <p>
 * Uma operação deste tipo é 
 * é construída com um operador {@link CRangeOperator}
 * que é aplicado aos valores de células de uma gama definida
 * por duas posições.
 * </p>
 * 
 * <p>
 * Exemplos:
 * </p>
 * <ol>
 * <li><code>SUM A1 A10</code> designa a soma dos valores das células
 * na coluna <code>A</code>, linhas <code>1</code> a <code>10</code>.</li>
 * <li><code>AVG A7 F7</code> designa a média dos valores das células
 * nas colunas <code>A</code> a <code>F</code>, coluna <code>7</code>.</li>
 * <li><code>MAX A1 B10</code>
 * designa o máximo dos valores no "rectângulo"
 * com "canto superior esquerdo" na posição <code>A1</code>
 * e "canto inferior direito" na posição <code>B10</code>.</li>
 * </ol>
 *
 */
public final class CRangeOperation implements NumberContent {

	/**
	 * operador a ser aplicado em todas as células entre start e finish
	 */
	private CRangeOperator rop;

	/**
	 * posiçaõ da célula onde se começa a aplicar o operador
	 */
	private CellPosition start;

	/**
	 * posiçaõ da célula onde se acaba da aplicar o operador
	 */
	private CellPosition finish;

	/**
	 * Construtor.
	 * @param op Operador.
	 * @param start Início da gama.
	 * @param end Fim da gama.
	 */
	public CRangeOperation(CRangeOperator op, CellPosition start, CellPosition end) {
		rop = op;
		this.start = start;
		finish = end;
	}

	/**
	 * Obtém fórmula.
	 * 
	 * @return String com o formato <code>op start end</code>.
	 */
	@Override
	public String formula() {
		return rop + " " + start.toString() + " " + finish.toString(); // TODO
	}

	/** 
	 * Avalia conteúdo.
	 *
	 * <p>
	 * A avaliação resulta de
	 * aplicar o operador de gama à lista
	 * à gama de células  <code>c = (pos,content)</code> que obedece
	 * às seguintes 3 condições:
	 * </p>
	 * <ol>
	 *  <li>
	 *  A coluna de <code>pos</code> está 
	 *  entre as colunas de <code>start</code> e <code>end</code>;
	 *  </li>
	 *  <li>
	 *  A linha de <code>pos</code> está 
	 *  entre as linhas de <code>start</code> e <code>end</code>;
	 *  </li> 
	 *  <li>
	 *  e {@code content} é um objecto do tipo {@link NumberContent}.
	 *  </li>
	 * </ol>
	 * <p>
	 * Se a lista de conteúdos nessas condições for vazia,
	 * deve ser retornado {@link Double#NaN}.
	 * Se a lista não for vazia, deve ser retornado o valor de aplicar o operador
	 * {@link CRangeOperator} à lista 
	 * usando o método {@link CRangeOperator#evaluate(java.util.List) CRangeOperator.evaluate(List&lt;Double&gt;)}.
	 * </p>
	 * <p>
	 * <b>Nota:</b> para uma implementação (mais) eficiente
	 * pode considerar o uso dos métodos
	 * {@link NavigableMap#ceilingEntry}
	 * e {@link NavigableMap#higherEntry(Object)}.
	 * </p>
	 * 
	 * @return Valor de avaliação (ver descrição acima).
	 */
	@Override
	public Double evaluate(CellValues cv) {
		
		if (start.compareTo(finish) == -1) {
				
			List<Double> lista = new ArrayList<>();
			
			for (Entry<CellPosition, CellContent> elem : cv.allCells().subMap(start, true, finish, true).entrySet()) {
				if (elem.getKey().getRow() >= start.getRow() && elem.getKey().getRow() <= finish.getRow() 
						&& elem.getValue() instanceof NumberContent) {
					lista.add(((NumberContent)elem.getValue()).evaluate(cv));
				}
			}
			
			if (lista.size() > 0) {
				return rop.evaluate(lista);
			}
		}
		return Double.NaN;
		
	}


}
