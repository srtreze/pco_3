package ycel.data;

/**
 * Conteúdo de célula definido por um número. 
 */
public final class CNumber implements NumberContent {

	/**
	 * valor double da célula
	 */
	private double valor;

	/**
	 * Construtor.
	 * @param value Valor a representar.
	 */
	public CNumber(double value) {
		valor = value;
	}

	/**
	 * Obtém fórmula.
	 * @return String representando o número.
	 * @see String#valueOf(double)
	 */
	@Override
	public String formula() {
		return String.valueOf(valor);
	}

	/**
	 * Avalia conteúdo.
	 * @return Número representado.
	 */
	@Override
	public Double evaluate(CellValues cv) {
		return valor;
	}

}
