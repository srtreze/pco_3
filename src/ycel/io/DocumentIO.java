package ycel.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;

import java.util.Map.Entry;

import ycel.data.CellContent;
import ycel.data.CellStyle;
import ycel.data.Document;
import ycel.data.CellPosition;

/**
 * Classe utilitária para input/output.
 *
 */
public final class DocumentIO {

	/**
	 * Carrega documento a partir de ficheiro.
	 * 
	 * <p>
	 * <b>Nota</b>: use {@link CellParser#parsePosition(String)}
	 * para ler posições, {@link CellParser#parseContent(Scanner)}
	 * para ler conteúdos, e {@link CellParser#parseStyle(String)}
	 * para ler estilos.
	 * </p>
	 * @param f Ficheiro.
	 * @return Documento lido.
	 * @throws FileNotFoundException se ficheiro não existe.
	 * @throws YCelIOException se houver um erro na leitura do ficheiro.
	 */
	public static Document load(File f) throws FileNotFoundException, YCelIOException {
		
		Scanner in;
		
		if(f.exists()){
			in = new Scanner(f);
		} else {
			throw new FileNotFoundException("O ficheiro " + f + " não foi encontrado");
		}
		
		in.useLocale(Locale.ENGLISH);

		try {
			
			Document doc = new Document();
			
			int num_linhas = in.nextInt();
			
			for(int conta_linhas = 0; conta_linhas < num_linhas; conta_linhas++){
				doc.setContent(CellParser.parsePosition(in.next()), CellParser.parseContent(in));
			}
			
			int num_style = in.nextInt();
			
			for(int conta_linhas = 0; conta_linhas < num_style; conta_linhas++){
				doc.setStyle(CellParser.parsePosition(in.next()), CellParser.parseStyle(in.next()));
			}
			return doc;
			
		} catch (Throwable e) {
			e.printStackTrace(System.err);
			throw new YCelIOException("File load error", e);
			
		} finally {
			in.close();
		}

	}

	/**
	 * Grava documento em ficheiro.
	 * @param f Ficheiro.
	 * @param doc Documento
	 * @throws FileNotFoundException se directório do ficheiro não existe.
	 */
	public static void save(File f, Document doc) throws FileNotFoundException {
		
		PrintStream out;
		
		try{
			out = new PrintStream(f);
		} catch (Throwable e){
			throw new FileNotFoundException("O ficheiro " + f + " não pode ser criado");
		}
		
		try { 
			
			out.append(doc.allCells().size()+"\n");
			for( Entry<CellPosition,CellContent> cell : doc.allCells().entrySet()) {
				out.append(cell.getKey().toString() + " " + cell.getValue().formula()+"\n");
			}
			
			out.append(doc.allStyles().size()+"\n");
			for( Entry<CellPosition,CellStyle> cell : doc.allStyles().entrySet()) {
				out.append(cell.getKey().toString() + " " + cell.getValue().toString()+"\n");
			}			
			
		} catch (Throwable e) {
			e.printStackTrace(System.err);
			throw new YCelIOException("File save error", e);
		} finally {
			out.close();
		}
	}

	/**
	 * Construtor privado, para inibir instanciação
	 * (conforme padrão "utility class").
	 */
	private DocumentIO() {

	}
}
